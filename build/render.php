<?php
$orderDirection = $_GET['order'] ?? "desc";
/**
 * @see https://github.com/WordPress/gutenberg/blob/trunk/docs/reference-guides/block-api/block-metadata.md#render
 */
$posts = get_posts([
    'post_type' => 'faq',
    'orderby' => 'date',
    'order' => $orderDirection
]);
$posts = array_filter($posts, function ($post) {
    $metadata = get_post_meta($post->ID); 
    return empty($metadata['showing']) || $metadata['showing'][0];
});
?>

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

<div style="text-align: right">
    <?php if ($orderDirection == "asc"): ?>
        <a href="?order=desc">Order by date desc</a>
    <?php else: ?>
        <a href="?order=asc">Order by date asc</a>
    <?php endif; ?>
</div>

<div class="accordion" id="accordionExample">
<?php foreach ($posts as $postKey => $post): ?>
    <?php 
        $metadata = get_post_meta($post->ID); 
        if (empty($metadata['question']) || empty($metadata['answer']) || (!empty($metadata['showing']) && false == $metadata['showing'][0])) {
            continue;
        }
        $openPost = "asc" == $orderDirection ? 0 : sizeof($posts) - 1;
    ?>

    <div class="accordion-item">
        <h2 class="accordion-header" id="heading-<?=$postKey?>">
        <button class="accordion-button <?= ($postKey != $openPost) ? "collapsed" : "" ?>" type="button" data-bs-toggle="collapse" data-bs-target="#collapse-<?=$postKey?>" aria-expanded="true" aria-controls="collapse-<?=$postKey?>">
                <?= $metadata['question'][0] ?>
            </button>
        </h2>
        <div id="collapse-<?=$postKey?>" class="accordion-collapse collapse <?= ($postKey == $openPost) ? "show" : "" ?>" aria-labelledby="heading-<?=$postKey?>" data-bs-parent="#accordionExample">
            <div class="accordion-body">
                <?= $metadata['answer'][0] ?>
            </div>
        </div>
    </div>
<?php endforeach; ?>
</div>
